# Backend приложения "Календарь"
**ДИСКЛЕЙМЕР**
____
До этого я бекенд на питоне не писал, flask тоже раньше не использовал. Много работал с бекендом на C++, реализовывал API используя проприетарный веб сервер.  Поэтому в коде будет много странностей, так как что-то делал не так, как принято или не использовал те возможности, про которые все знают. Кроме того, возможны ошибки, так как времени на тестирование и отладку было очень мало.
____
**Реализованы обязательные требования:**
* Создать пользователя;
* Создать встречу в календаре пользователя со списком приглашенных пользователей;
* Получить детали встречи;
* Принять или отклонить приглашение другого пользователя;
* Найти все встречи пользователя для заданного промежутка времени;
* Для заданного списка пользователей и минимальной продолжительности встречи, найти ближайшей интервал времени, в котором все эти пользователи свободны.
Я реализовал поиск всех подходящих для данных условий интервалов.
* У встреч в календаре должна быть возможна настройка повторов. В повторах нужно поддержать все возможности, доступные в Google-календаре, кроме Сustom.

**Дополнительный функционал:**
* Поддержка видимости встреч (если встреча приватная, другие пользователи могут получить только информацию о занятости пользователя, но не детали встречи);
* Настройки рабочего времени пользователя, использование этих настроек для поиска интервала времени, в котором участники свободны;
* Проверка запросов пользователя на соответствие схемы, если запрос неверный, то возвращается ошибка 400 (Bad Request), сами схемы можно посмотреть в файле `schemas.py`;
* Хранение данных о встречах и пользователях в базе данных (для локального запуска более чем достаточно sqlite3 с сохранением в файл);
* Авторизация пользователся реализована условно, данные о пользователе, который выполняет запрос передаются через cgi параметр `user_uuid` в те ручки API, в которых теоретически важна аутентификация;
* Интеграционные тесты;
* Кеширование пользователей и встреч;

**Что дополнительно стоит реализовать (не хватило времени):**
* Проверка данных от клиента на sql инъекции;
* Повысить функциональность кеширования, выгружать неиспользованные данные, кешировать календари и данные об участниках встреч;
* При созданиии встречи проверять рабочее время и занятость приглашенных. Сейчас встреча создается в любом случае. Подразумевается, что если пользователь
хочет создать оптимальную по времени встречу, он сначала сделает запрос в `GET /find_best_time/`, и на основании предложенного времени назначит встречу.

# Запуск приложения
Установить необходимые зависимости, если они не установлены:
```
pip install -U Flask
pip install -U Flask-RESTful
pip install -U python-dateutil
pip install -U jsonschema
```
Запустить приложение:
```
python3 run.py [PORT]
```
Например:
```
python3 run.py 5000
```
Приложение запустится на адресе `http://127.0.0.1:5000`. К нему можно отправлять HTTP запросы описанные в API ниже.

Запустить интеграционные тесты:
```
python3 tests.py
```


# API

## POST /user/create/
Create user
### Request body
```
{
    "name" : "User Name",
    "work_time_start" : "10:55:00", // optional, time format hh:mm:ss
    "work_time_end" : "10:55:00" // optional, time format hh:mm:ss
}
```
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK, user successfully created | **Content-Type**:</br>text/json |
| 400 | Client error, invalid request |**Content-Type**:</br>text/json |
### Response body (if successful)
```
{
    "user_uuid" : "52eb837a-aa7b-49d2-bd40-b4e015560079"
}
```

## DELETE /user/\<user_uuid\>
Delete user
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK, user successfully deleted | **Content-Type**:</br>text/json |
| 404 | User not found |**Content-Type**:</br>text/json |

## GET /user/\<user_uuid\>
Get user information
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK | **Content-Type**:</br>text/json |
| 404 | User not found |**Content-Type**:</br>text/json |
### Response body (if successful)
```
{
    "name" : "User Name",
    "uuid" : "user_uuid",
    "work_time_start" : "10:55:00", // optional, time format hh:mm:ss
    "work_time_end" : "10:55:00" // optional, time format hh:mm:ss
}
```

## POST /meeting/create/
Create meeting
### Request body
```
{
    "name" : "Meeting Name",
    "date" : "2022-10-21",
    "start_time" : "10:55:00",
    "duration" : 3600, // integer seconds
    "creator" : "creator_uuid",
    "participants" : [
        "user_uuid1",
        "user_uuid2"
    ],
    "privacy" : "PUBLIC", //optional one of (PUBLIC, PRIVATE)
    "repeat" : "ONCE", //optional one of (ONCE, DAILY, WEEKLY, MONTHLY, YEARLY)
}
```
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK, meeting successfully created | **Content-Type**:</br>text/json |
| 400 | Client error, invalid request |**Content-Type**:</br>text/json |
### Response body (if successful)
```
{
    "meeting_uuid" : "52eb837a-aa7b-49d2-bd40-b4e015560079"
}
```
## DELETE /meeting/\<meeting_uuid\>
Delete meeting
### Request params
| parameter name | type | description |
|---|---|---|
| `user_uuid` | string | uuid of user, who wants to delete meeting |
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK, meeting successfully deleted | **Content-Type**:</br>text/json |
| 404 | Meeting not found or hidden |**Content-Type**:</br>text/json |

## GET /meeting/\<meeting_uuid\>
Get meeting information
### Request params
| parameter name | type | description |
|---|---|---|
| `user_uuid` | string | uuid of user, who wants to retrive meeting information |
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK | **Content-Type**:</br>text/json |
| 404 | Meeting not found |**Content-Type**:</br>text/json |
### Response body (if user have permission for this meeting)
```
{
    "name" : "Meeting Name",
    "date" : "2022-10-21",
    "start_time" : "10:55:00",
    "uuid" : "meeting_uuid",
    "duration" : 3600, // integer seconds
    "privacy" : "PUBLIC", //optional one of (PUBLIC, PRIVATE)
    "repeat" : "ONCE", //optional one of (ONCE, DAILY, WEEKLY, MONTHLY, YEARLY)
    "creator" : "creator_uuid",
    "participants" : [
        {
            "user_uuid" : "creator_uuid",
            "participation_status" : "YES", // NO_ANSWER/YES/NO/MAYBE
            "role" : "CREATOR" // CREATOR / MEMBER
        },
        {
            "user_uuid" : "some_user_uuid1",
            "participation_status" : "YES", // NO_ANSWER/YES/NO/MAYBE
            "role" : "MEMBER" // CREATOR / MEMBER
        },
        {
            "user_uuid" : "some_user_uuid1",
            "participation_status" : "YES", // NO_ANSWER/YES/NO/MAYBE
            "role" : "MEMBER" // CREATOR / MEMBER
        }
    ]
}
```
### Response body (if user have no permission for this meeting)
```
{
    "date" : "2022-10-21",
    "start_time" : "10:55:00",
    "duration" : 3600, // integer seconds
    "privacy" : "PRIVATE"
}
```

## POST /meeting/\<meeting_uuid\>/change_participation
Accept / deny meeting invintation
### Request params
| parameter name | type | description |
|---|---|---|
| `user_uuid` | string | uuid of user |
### Request body
```
{
    "status" : "YES", // YES / NO / MAYBE
}
```
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK | **Content-Type**:</br>text/json |
| 404 | Meeting not found or You have not been invited |**Content-Type**:</br>text/json |


## GET /user/\<user_uuid\>/calendar/
Get user calendar for specified time interval
### Request params
| parameter name | type | description |
|---|---|---|
| `start_datetime` | int | start of time interval in seconds since epoch |
| `end_datetime` | int | end of time interval in seconds since epoch|
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK | **Content-Type**:</br>text/json |
| 404 | User not found |**Content-Type**:</br>text/json |
### Response body
```
{
    "calendar" : [
        {
            "name" : "meeting name",
            "uuid" : "meeting_uuid",
            "date" : "2022-11-02",
            "start_time" : "10:00:00",
            "duration" : 3600 // seconds
        },
        ...
    ]
}
```

## GET /find_best_time/
Find best time to hold meeting in specified time interval
### Request body
```
{
    "participants" : [
        "user_uuid_1",
        "user_uuid_2",
        "user_uuid_3"
    ],
    "from" : 16090909, // interval start timestamp
    "to" : 17090909, // interval end timestamp
    "duration" : 3600 // integer meeting duration in seconds
}
```
### Response codes
| response code  | comment | header |
| --- | --- | --- |
| 200 | OK | **Content-Type**:</br>text/json |
| 404 | Time not found |**Content-Type**:</br>text/json |
### Response body (if successful)
```
{   
    "best_times" :
    [
        {
            "date" : "2022-10-21",
            "start_time" : "10:55:00",
            "max_duration" : 3600 // integer seconds
        },
        ...
    ]
}
```
