from app import create_app
import sys

if __name__ == "__main__":
    app = create_app()
    port = 5000
    try:
        port = int(sys.argv[1])
    except Exception:
      pass
    app.run(debug=True, port=port)
