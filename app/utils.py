import datetime
from dateutil.relativedelta import relativedelta
from app.models import RepeatType, CalendarValue, BestTimeValue


def step_time(date : datetime.datetime, repeat : RepeatType) -> datetime.datetime:
    if repeat == RepeatType.DAILY:
        date += datetime.timedelta(days=1)
    elif repeat == RepeatType.WEEKLY:
        date += datetime.timedelta(weeks=1)
    elif repeat == RepeatType.MONTHLY:
        date += relativedelta(months=1)
    elif repeat == RepeatType.YEARLY:
        date += relativedelta(years=1)
    return date


def construct_user_calendar(interval_start : int, interval_end : int, user_meetings : list, sort : bool = True) -> list:
    result = []
    interval_start = datetime.datetime.fromtimestamp(interval_start)
    interval_end = datetime.datetime.fromtimestamp(interval_end)
    for meeting in user_meetings:
        meeting_start_datetime = datetime.datetime.combine(meeting.date, meeting.start_time)
        meeting_end_datetime = meeting_start_datetime + meeting.duration

        if meeting.repeat == RepeatType.ONCE:
            if meeting_end_datetime < interval_start:
                continue
            elif meeting_end_datetime > interval_start and meeting_start_datetime < interval_end:
                result.append(
                    CalendarValue(
                        name=meeting.name,
                        uuid=meeting.uuid,
                        start_datetime=meeting_start_datetime,
                        duration=meeting.duration
                    )
                )
                continue

        while meeting_end_datetime < interval_start:
            meeting_start_datetime = step_time(meeting_start_datetime, meeting.repeat)
            meeting_end_datetime = step_time(meeting_end_datetime, meeting.repeat)
        
        while meeting_start_datetime < interval_end:
            result.append(
                    CalendarValue(
                        name=meeting.name,
                        uuid=meeting.uuid,
                        start_datetime=meeting_start_datetime,
                        duration=meeting.duration
                    )
                )
            meeting_start_datetime = step_time(meeting_start_datetime, meeting.repeat)
            meeting_end_datetime = step_time(meeting_end_datetime, meeting.repeat)
    if sort:
        result.sort(key = lambda x: x.start_datetime)
    return result

def time_diff(a : datetime.time, b : datetime.time) -> datetime.timedelta:
    dateTimeA = datetime.datetime.combine(datetime.date.today(), a)
    dateTimeB = datetime.datetime.combine(datetime.date.today(), b)
    return dateTimeA - dateTimeB

def set_time(a : datetime.datetime, b : datetime.time) -> datetime.datetime:
    return datetime.datetime.combine(a.date(), b)

# finding best time for meeting
def find_best_time(participants : list, calendars : list, from_timestamp : int, to_timestamp : int, duration : int) -> list | None:
    result = []
    time_window = [datetime.time.min, datetime.time.max]
    need_duration = datetime.timedelta(seconds=duration)
    # here we merging all participants non working time
    for participant in participants:
        time_start = participant.work_time_start
        time_end = participant.work_time_end
        if time_start > time_window[0]:
            time_window[0] = time_start
        if time_end < time_window[1]:
            time_window[1] = time_end
    if time_diff(time_window[1], time_window[0]) < need_duration:
        return result

    # merging all users calendar meeting to one array
    merged_calendar = []
    for calendar in calendars:
        merged_calendar += calendar

    from_datetime = datetime.datetime.fromtimestamp(from_timestamp)
    to_datetime = datetime.datetime.fromtimestamp(to_timestamp)

    # now we assume that non working time is smth like meeting, and merge this time into calendar like fake meetings
    cur_from = from_datetime
    while cur_from < to_datetime:
        # firstly evening time
        start_datetime = cur_from
        start_datetime = set_time(start_datetime, time_window[1]) # end of the working day
        duration = time_diff(datetime.time.max, time_window[1]) # time since end of working day till end of day
        merged_calendar.append(CalendarValue(
            name='fake name',
            uuid='fake_uuid',
            start_datetime=start_datetime,
            duration=duration
        ))
        # then morning time
        start_datetime = cur_from
        start_datetime = set_time(start_datetime, datetime.time.min) # 00:00
        duration = time_diff(time_window[0], datetime.time.min) # from 00:00 till start of working time
        merged_calendar.append(CalendarValue(
            name='fake name',
            uuid='fake_uuid',
            start_datetime=start_datetime,
            duration=duration
        ))
        cur_from += datetime.timedelta(days=1)

    # sorting calendar
    merged_calendar.sort(key = lambda x: x.start_datetime)
    reversed_calendar = sorted(merged_calendar, key = lambda x: (x.start_datetime + x.duration))

    if len(merged_calendar) == 0:
        # just use all time between  two borders
        result.append(BestTimeValue(
            start_datetime=from_datetime,
            max_duration=(to_datetime - from_datetime)
        ))
    else:
        # time between start of interval and first meeting
        first_meeting_start = merged_calendar[0].start_datetime
        possible_duration = first_meeting_start - from_datetime
        if possible_duration >= need_duration:
            result.append(BestTimeValue(
                start_datetime=from_datetime,
                max_duration=(possible_duration)
            ))
        # time between last meeting and end of interval
        last_meeting = reversed_calendar[-1]
        last_meeting_end = last_meeting.start_datetime + last_meeting.duration
        possible_duration = to_datetime - last_meeting_end
        if possible_duration >= need_duration:
            result.append(BestTimeValue(
                start_datetime=last_meeting_end,
                max_duration=possible_duration
            ))
        if len(merged_calendar) >= 2:
            for i in range(0, len(merged_calendar) - 1):
                cur_meeting = merged_calendar[i]
                next_meeting = merged_calendar[i + 1]
                cur_meeting_end = cur_meeting.start_datetime + cur_meeting.duration
                next_meeting_start = next_meeting.start_datetime
                possible_duration = next_meeting_start - cur_meeting_end
                if possible_duration >= need_duration:
                    result.append(BestTimeValue(
                        start_datetime=cur_meeting_end,
                        max_duration=possible_duration
                    ))
    return result
