-- DROP TABLE IF EXISTS users;
-- DROP TABLE IF EXISTS meetings;
-- DROP TABLE IF EXISTS user_meetings;

CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    uuid varchar(40) UNIQUE NOT NULL CHECK(uuid <> ''),
    name TEXT NOT NULL,
    work_time_start varchar(40) NOT NULL,
    work_time_end varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS meetings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    uuid varchar(40) UNIQUE NOT NULL CHECK(uuid <> ''),
    name TEXT NOT NULL,
    creator varchar(40) NOT NULL CHECK(creator <> ''),
    date DATE NOT NULL,
    start_time TIME NOT NULL,
    duration INTEGER NOT NULL,
    privacy varchar(40) NOT NULL,
    repeat varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS user_meetings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_uuid varchar(40) NOT NULL CHECK(user_uuid <> ''),
    meeting_uuid varchar(40) NOT NULL CHECK(meeting_uuid <> ''),
    user_role varchar(40) NOT NULL,
    participation_status varchar(40) NOT NULL
);