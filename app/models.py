from enum import Enum
import datetime
import uuid

class Model:
    def json(self):
        pass

    def insert_query(self, table):
        js = self.json()
        query = f'INSERT INTO {table} ('
        query += ', '.join([str(key) for key in js.keys()]) 
        query += ') VALUES (\''
        query += '\', \''.join([str(value) for value in js.values()])
        query += '\');'
        return query

class BaseEnum(Enum):
    def __str__(self):
        return f'{self.name}'

class RepeatType(BaseEnum):
    ONCE = 'ONCE'
    DAILY = 'DAILY'
    WEEKLY = 'WEEKLY'
    MONTHLY = 'MONTHLY'
    YEARLY = 'YEARLY'

class PrivacyType(BaseEnum):
    PUBLIC = 'PUBLIC'
    PRIVATE = 'PRIVATE'

class UserRole(BaseEnum):
    CREATOR = 'CREATOR'
    MEMBER = 'MEMBER'

class ParticipationStatus(BaseEnum):
    NO_ANSVER = 'NO_ANSVER'
    YES = 'YES'
    NO = 'NO'
    MAYBE = 'MAYBE'

# working_time here in utc+0 
class User(Model):
    def __init__(
            self,
            uuid : str,
            name : str,
            work_time_start : datetime.time = datetime.time(hour=0),
            work_time_end : datetime.time = datetime.time(hour=23, minute=59, second=59)
    ):
        self.uuid = uuid
        self.name = name
        self.work_time_start = work_time_start
        self.work_time_end = work_time_end
    
    def json(self):
        result = {
            'uuid' : self.uuid,
            'name' : self.name,
            'work_time_start' : self.work_time_start.isoformat(timespec='seconds'),
            'work_time_end' : self.work_time_end.isoformat(timespec='seconds'),
        }
        return result
    
    @staticmethod
    def from_row(user_data):
        name = user_data['name']
        if 'uuid' not in user_data:
            user_uuid = str(uuid.uuid4())
        else:
            user_uuid = user_data['uuid']
        user = User(name=name, uuid=user_uuid)
        if 'work_time_start' in user_data:
            user.work_time_start = datetime.datetime.strptime(user_data['work_time_start'], '%H:%M:%S').time()
        if 'work_time_end' in user_data:
            user.work_time_end = datetime.datetime.strptime(user_data['work_time_end'], '%H:%M:%S').time()
        return user

# here start time and end time in hours utc+0
class Meeting(Model):
    def __init__(
            self,
            uuid : str,
            name : str,
            creator : str,
            date : datetime.date,
            start_time : datetime.time,
            duration : datetime.timedelta,
            privacy : PrivacyType = PrivacyType.PUBLIC,
            repeat : RepeatType = RepeatType.ONCE
    ):
        self.uuid = uuid
        self.name = name
        self.creator = creator
        self.date = date
        self.start_time = start_time
        self.duration = duration
        self.privacy = privacy
        self.repeat = repeat
    
    def json(self):
        result = {
            'uuid' : self.uuid,
            'name' : self.name,
            'creator' : self.creator,
            'date' : self.date.isoformat(),
            'start_time' : self.start_time.isoformat(timespec='seconds'),
            'duration' : self.duration.seconds,
            'privacy' : str(self.privacy),
            'repeat' : str(self.repeat),
        }
        return result
    
    def format_response(self, user_uuid, participants):
        meeting_js = self.json()
        if self.privacy == PrivacyType.PUBLIC or self.creator == user_uuid or user_uuid in participants:
            meeting_js['participants'] = []
            for participant in participants.values():
                meeting_js['participants'].append(participant.json())
            return meeting_js
        else:
            result = {}
            result['date'] = meeting_js['date']
            result['start_time'] = meeting_js['start_time']
            result['duration'] = meeting_js['duration']
            result['repeat'] = meeting_js['repeat']
            return result

    @staticmethod
    def from_row(meeting_data):
        meeting_data = dict(meeting_data)
        name = meeting_data['name']
        if 'uuid' in meeting_data:
            meeting_uuid = meeting_data['uuid']
        else:
            meeting_uuid = str(uuid.uuid4())
        return Meeting(
            name=name,
            uuid=meeting_uuid,
            creator=meeting_data['creator'],
            date=datetime.date.fromisoformat(meeting_data['date']),
            start_time=datetime.datetime.strptime(meeting_data['start_time'], '%H:%M:%S').time(),
            duration=datetime.timedelta(seconds=int(meeting_data['duration'])),
            privacy=PrivacyType[meeting_data['privacy']],
            repeat=RepeatType[meeting_data['repeat']]
        )

class MeetingParticipant(Model):
    def __init__(
        self,
        user_uuid : str,
        meeting_uuid : str,
        user_role : UserRole = UserRole.MEMBER,
        participation_status : ParticipationStatus = ParticipationStatus.NO_ANSVER
    ):
        self.user_uuid = user_uuid
        self.meeting_uuid = meeting_uuid
        self.user_role = user_role
        self.participation_status = participation_status
    
    def json(self):
        result = {
            'user_uuid' : self.user_uuid,
            'meeting_uuid' : self.meeting_uuid,
            'user_role' : str(self.user_role),
            'participation_status' : str(self.participation_status)
        }
        return result

    
    @staticmethod
    def insert_query(participants, table):
        query = f'INSERT INTO {table} (user_uuid, meeting_uuid, user_role, participation_status) VALUES '
        
        i = 0
        for participant in participants:
            js = participant.json()
            if i != 0:
                query += ','
            query += '(\''
            query += '\', \''.join([str(value) for value in js.values()])
            query += '\')'
            i += 1
        query += ';'
        return query

    @staticmethod
    def from_row(data):
        user_uuid = data['user_uuid']
        meeting_uuid = data['meeting_uuid']
        user_role = data['user_role']
        participation_status = data['participation_status']
        
        return MeetingParticipant(
            user_uuid=user_uuid,
            meeting_uuid=meeting_uuid,
            user_role=UserRole[user_role],
            participation_status=ParticipationStatus[participation_status]
        )
    
class CalendarValue(Model):
    def __init__(
        self,
        name : str,
        uuid : str,
        start_datetime : datetime.datetime,
        duration : datetime.timedelta,
    ):
        self.name = name
        self.uuid = uuid
        self.start_datetime = start_datetime
        self.duration = duration

    def json(self):
        result = {
            'name' : self.name,
            'uuid' : self.uuid,
            'start_datetime' : self.start_datetime.isoformat(timespec='seconds'),
            'duration' : self.duration.seconds
        }
        return result

class BestTimeValue(Model):
    def __init__(
        self,
        start_datetime : datetime.date,
        max_duration : datetime.timedelta,
    ):
        self.start_datetime = start_datetime
        self.max_duration = max_duration

    def json(self):
        result = {
            'start_datetime' : self.start_datetime.isoformat(timespec='seconds'),
            'max_duration' : self.max_duration.seconds,
        }
        return result

