from flask import Flask
import flask_restful
from app.resources import all_resources
from app.db import DBManager


def create_app():
    app = Flask(__name__)
    api = flask_restful.Api(app)

    database_manager = DBManager()
    for cls in all_resources:
        print(f'Add resource {cls.route}')
        api.add_resource(cls, f'/api/v1{cls.route}', resource_class_args=([database_manager]))

    return app