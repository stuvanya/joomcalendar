from jsonschema import validate

time_pattern = '^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$'
date_pattern = '^20[0-9][0-9]-(0[1-9]|1[0-2])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])$'

user_add_schema = {
    'type' : 'object',
    'additionalProperties': False,
    'properties' : {
        'name' : {'type' : 'string'},
        'work_time_start' : {
            'type' : 'string',
            'pattern' : time_pattern
        },
        'work_time_end' : {
            'type' : 'string',
            'pattern' : time_pattern
        }
    },
    'required' : ['name']
}

def validate_user_add(data):
    validate(instance=data, schema=user_add_schema)

meeting_add_schema = {
    'type' : 'object',
    'additionalProperties': False,
    'properties' : {
        'name' : {'type' : 'string'},
        'date' : {
             'type' : 'string',
             'pattern' : date_pattern
        },
        'start_time' : {
            'type' : 'string',
            'pattern' : time_pattern
        },
        'duration' : { 
            'type' : 'integer',
            'minimum' : 1,
        },
        'creator' : { 'type' : 'string' },
        'participants' : {
            'type' : 'array',
            'items': {
                'type': 'string',
            }
        },
        'privacy' : {
            'type' : 'string',
            'pattern' : '^(PUBLIC|PRIVATE)$',
            'default' : 'PUBLIC'
        },
        'repeat' : {
            'type' : 'string',
            'pattern' : '^(ONCE|DAILY|WEEKLY|MONTHLY|YEARLY)$',
            'default' : 'ONCE'
        }
    },
    'required' : ['name', 'date', 'start_time', 'duration', 'participants']
}

def validate_meeting_add(data):
    validate(instance=data, schema=meeting_add_schema)

participation_schema = {
    'type' : 'object',
    'additionalProperties': False,
    'properties' : {
        'status' : {
            'type' : 'string',
            'pattern' : '^(YES|NO|MAYBE)$'
        }
    },
    'required' : ['status']
}

def validate_participation(data):
    validate(instance=data, schema=participation_schema)

find_best_time_schema = {
    'type' : 'object',
    'additionalProperties': False,
    'properties' : {
        'from' : { 'type' : 'integer' },
        'to' : { 'type' : 'integer' },
        'duration' : {
            'type' : 'integer',
            'minimum' : 1,
        },
        'participants' : {
            'type' : 'array',
            'items': {
                'type': 'string',
            }
        }
    },
    'required' : ['from', 'to', 'duration', 'participants']
}

def validate_find_best_time(data):
    validate(instance=data, schema=find_best_time_schema)
