from abc import ABCMeta, abstractproperty
from flask_restful import Resource as BaseResource
from flask import Response, request
import json
from jsonschema import ValidationError
from app.schemas import validate_user_add, validate_meeting_add, validate_participation, validate_find_best_time


class Resource(BaseResource):
    __metaclass__ = ABCMeta

    def __init__(self, db_manager):
        self.db_manager = db_manager

    @abstractproperty
    def route(self):
        pass

def invalid_response():
    return Response(json.dumps({"error" : "Client error, invalid request"}), status=400, mimetype='application/json')

def not_found_response():
    return Response(json.dumps({"error" : "Not found"}), status=404, mimetype='application/json')

def ok_response(data):
    return Response(json.dumps(data), status=200, mimetype='application/json')

class CreateUser(Resource):
    route = '/user/create/'
    def post(self):
        user_data = request.get_json(force=True)
        try:
            validate_user_add(user_data)
            user_uuid = self.db_manager.create_user(user_data)
            return ok_response({"user_uuid" : user_uuid})
        except (KeyError, ValidationError) as e:
            print(e)
            return invalid_response()

class DeleteUser(Resource):
    route = '/user/<user_uuid>/'
    def delete(self, user_uuid):
        if self.db_manager.delete_user(user_uuid):
            return ok_response({"message" : "User successfully deleted"})
        else:
            return not_found_response()

class GetUser(Resource):
    route = '/user/<user_uuid>/'
    def get(self, user_uuid):
        user = self.db_manager.get_user(user_uuid)
        if user is None:
            return not_found_response()
        return ok_response(user.json())

class CreateMeeting(Resource):
    route = '/meeting/create/'
    def post(self):
        meeting_data = request.get_json(force=True)
        try:
            validate_meeting_add(meeting_data)
            meeting_uuid = self.db_manager.create_meeting(meeting_data)
            return ok_response({"meeting_uuid" : meeting_uuid})
        except (KeyError, ValidationError) as e:
            return invalid_response()

class GetMeeting(Resource):
    route = '/meeting/<meeting_uuid>/'
    def get(self, meeting_uuid):
        user_uuid = request.args['user_uuid']
        meeting = self.db_manager.get_meeting(meeting_uuid)
        if meeting is None:
            return not_found_response()
        participants = self.db_manager.get_meeting_participants(meeting_uuid)
        return ok_response(meeting.format_response(user_uuid, participants))

class DeleteMeeting(Resource):
    route = '/meeting/<meeting_uuid>/'
    def delete(self, meeting_uuid):
        user_uuid = request.args['user_uuid']
        result = self.db_manager.delete_meeting(meeting_uuid, user_uuid)
        if result:
            return ok_response({"message" : "Meeting successfully deleted"})
        else:
            return not_found_response()

class ChangeParticipation(Resource):
    route = '/meeting/<meeting_uuid>/change_participation/'
    def post(self, meeting_uuid):
        user_uuid = request.args['user_uuid']
        participation_data = request.get_json(force=True)
        try:
            validate_participation(participation_data)
            result = self.db_manager.change_participation(meeting_uuid, user_uuid, participation_data['status'])
            if result:
                return ok_response({"message" : "Participation status successfully updated"})
            else:
                return not_found_response()
        except (KeyError, ValidationError) as e:
            return invalid_response()

class UserCalendar(Resource):
    route = '/user/<user_uuid>/calendar/'
    def get(self, user_uuid):
        start_datetime = request.args['start_datetime']
        end_datetime = request.args['end_datetime']
        try:
            return ok_response(self.db_manager.get_calendar(user_uuid, start_datetime, end_datetime))
        except ValueError:
            return invalid_response()

class FindBestTime(Resource):
    route = '/find_best_time/'
    def get(self):
        data = request.get_json(force=True)
        try:
            validate_find_best_time(data)
            result = self.db_manager.find_best_time(data['participants'], data['from'], data['to'], data['duration'])
            if result:
                return ok_response(result)
            else:
                return not_found_response()
        except (KeyError, ValidationError) as e:
            return invalid_response()


all_resources = [
    CreateUser,
    DeleteUser,
    GetUser,
    CreateMeeting,
    GetMeeting,
    DeleteMeeting,
    ChangeParticipation,
    UserCalendar,
    FindBestTime
]
