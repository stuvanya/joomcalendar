import sqlite3
from app.models import User, Meeting, MeetingParticipant, UserRole, ParticipationStatus
from app.utils import construct_user_calendar, find_best_time


class DBManager():
    def __init__(self):
        connection = DBManager.get_db_connection()
        with open('app/schema.sql') as f:
            connection.executescript(f.read())
        connection.commit()
        self.users_cache = dict()
        self.meetings_cache = dict()

    def create_user(self, user_data) -> str:
        user = User.from_row(user_data)
        
        self.users_cache[user.uuid] = user
        query = user.insert_query('users')

        connection = DBManager.get_db_connection()
        connection.cursor().execute(query)
        connection.commit()
        connection.close()
        return user.uuid

    def get_user(self, uuid) -> User | None:
        user = self.users_cache.get(uuid, None)
        if user is None:
            user = self._get_user_from_db(uuid)
        return user

    def _get_user_from_db(self, uuid) -> User | None:
        connection = DBManager.get_db_connection()
        row = connection.execute(f'SELECT * from users WHERE uuid=\'{uuid}\'').fetchone()
        if row is not None:
            user = User.from_row(row)
            self.users_cache[user.uuid] = user
            return user

    def delete_user(self, uuid) -> bool:
        self.users_cache.pop(uuid, None)
        return self._delete_user_from_db(uuid)
    
    def _delete_user_from_db(self, uuid) -> bool:
        query1 = f'DELETE FROM users WHERE uuid=\'{uuid}\''
        connection = DBManager.get_db_connection()
        cursor = connection.cursor()
        cursor.execute(query1)
        count = cursor.rowcount
        if count == 1:
            query2 = f'DELETE FROM meetings WHERE creator=\'{uuid}\';'
            cursor.execute(query2)
        connection.close()
        return count == 1

    def create_meeting(self, meeting_data) -> str:
        meeting = Meeting.from_row(meeting_data)
        self.meetings_cache[meeting.uuid] = meeting

        query = meeting.insert_query('meetings')
        connection = DBManager.get_db_connection()
        connection.cursor().execute(query)
        
        participants = []
        participants.append(MeetingParticipant(
            meeting_uuid=meeting.uuid,
            user_uuid=meeting.creator,
            user_role=UserRole.CREATOR,
            participation_status=ParticipationStatus.YES))

        participants_js = meeting_data['participants']
        for participant_id in participants_js:
            participants.append(MeetingParticipant(
                meeting_uuid=meeting.uuid,
                user_uuid=participant_id))

        participants_query = MeetingParticipant.insert_query(participants, 'user_meetings')
        connection.cursor().execute(participants_query)
        connection.commit()
        connection.close()

        return meeting.uuid
    
    def get_meeting(self, uuid) -> Meeting | None:
        meeting = self.meetings_cache.get(uuid, None)
        if meeting is None:
            meeting = self._get_meeting_from_db(uuid)
        return meeting
    
    def _get_meeting_from_db(self, uuid) -> Meeting | None:
        connection = DBManager.get_db_connection()
        row = connection.execute(f'SELECT * from meetings WHERE uuid=\'{uuid}\'').fetchone()
        if row is not None:
            meeting = Meeting.from_row(row)
            self.meetings_cache[meeting.uuid] = meeting
            return meeting
    
    def get_meeting_participants(self, meeting_uuid) -> dict:
        result = dict()
        connection = DBManager.get_db_connection()
        rows = connection.execute(f'SELECT * from user_meetings WHERE meeting_uuid=\'{meeting_uuid}\'').fetchall()
        for row in rows:
            participant = MeetingParticipant.from_row(row)
            result[participant.user_uuid] = participant
        return result
    
    def _get_user_meetings(self, user_uuid) -> list:
        result = list()
        connection = DBManager.get_db_connection()
        rows = connection.execute(f'SELECT * from user_meetings JOIN meetings ON user_meetings.meeting_uuid = meetings.uuid WHERE user_uuid=\'{user_uuid}\'').fetchall()
        for row in rows:
            result.append(Meeting.from_row(row))
        return result

    def delete_meeting(self, meeting_uuid, user_uuid) -> bool:
        if meeting_uuid in self.meetings_cache:
            if self.meetings_cache[meeting_uuid].creator == user_uuid:
                self.meetings_cache.pop(meeting_uuid)
        return self._delete_meeting_from_db(meeting_uuid, user_uuid)
    
    def _delete_meeting_from_db(self, meeting_uuid, user_uuid) -> bool:
        query = f'DELETE FROM meetings WHERE uuid=\'{meeting_uuid}\' AND creator=\'{user_uuid}\';'
        connection = DBManager.get_db_connection()
        cursor = connection.cursor()
        cursor.execute(query)
        count = cursor.rowcount
        if count != 0:
            query = f'DELETE FROM user_meetings WHERE meeting_uuid=\'{meeting_uuid}\';'
            cursor.execute(query)
        connection.commit()
        connection.close()
        return count != 0

    def change_participation(self, meeting_uuid, user_uuid, status) -> bool:
        query = f'UPDATE user_meetings SET participation_status=\'{status}\' WHERE user_uuid=\'{user_uuid}\' AND meeting_uuid=\'{meeting_uuid}\';'
        connection = DBManager.get_db_connection()
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
        connection.close()
        return cursor.rowcount != 0
    
    def get_calendar(self, user_uuid, start_datetime, end_datetime) -> dict:
        start = int(start_datetime)
        end = int(end_datetime)
        if end <= start:
            raise ValueError
        user_meetings = self._get_user_meetings(user_uuid)
        result = construct_user_calendar(start, end, user_meetings)
        return {'calendar' : [value.json() for value in result] }
    
    def find_best_time(self, participants, from_timestamp, to_timestamp, duration) -> dict | None:
        participants_models = []
        participants_calendars = []
        start = int(from_timestamp)
        end = int(to_timestamp)
        if end <= start:
            raise ValueError
        for participant_uuid in participants:
            model = self.get_user(participant_uuid)
            if model is None:
                raise ValueError
            participants_models.append(model)
            user_meetings = self._get_user_meetings(participant_uuid)
            participants_calendars.append(construct_user_calendar(start, end, user_meetings, False))
        result = find_best_time(participants_models, participants_calendars, from_timestamp, to_timestamp, duration)
        if len(result) == 0:
            return None
        return {'best_times' : [value.json() for value in result] }

    @staticmethod
    def get_db_connection():
        conn = sqlite3.connect('database.db')
        conn.row_factory = sqlite3.Row
        return conn
