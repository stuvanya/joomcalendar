import json
import unittest
from app import create_app
import datetime
import uuid


USER_CREATE_JSON_OK = {'name' : 'User Name', 'work_time_start' : '10:00:00'}
USER_CREATE_JSON_BAD = {'name' : 'User Name', 'work_time_start' : '25:00:00'}
MEETING_CREATE_JSON_OK = {'name' : 'Daily meeting', 'date' : '2022-10-22', 'start_time' : '10:55:00', 'duration' : 3600,
    'creator' : 'some_user_id', 'privacy' : 'PRIVATE', 'repeat' : 'DAILY', 'participants' : []}
MEETING_CREATE_JSON_BAD = {'name' : 'Daily meeting', 'date' : '2022-10-22', 'start_time' : '10:55:00', 'duration' : 3600,
    'creator' : 'some_user_id', 'privacy' : 'PABLIC', 'repeat' : 'DAILY', 'participants' : []}
FIND_BEST_TIME_JSON = {
    'participants' : [
    ],
    'from' : 1666386000,
    'to' : 1666558800,
    'duration' : 3600
}
API_URL = 'http://127.0.0.1:5000/api/v1'

def create_uuid():
    return str(uuid.uuid4())


class TestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app().test_client()

    def test_user_create(self):
        response = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_OK)
        self.assertEqual(response.status_code, 200)

        response = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_BAD)
        self.assertEqual(response.status_code, 400)
    
    def test_user_delete(self):
        response1 = self.app.delete(f'{API_URL}/user/dummy_user_uuid/')
        self.assertEqual(response1.status_code, 404)

        response2 = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_OK)
        user_uuid = json.loads(response2.data)['user_uuid']

        response3 = self.app.delete(f'{API_URL}/user/{user_uuid}/')
        self.assertEqual(response3.status_code, 200)
    
    def test_user_get(self):
        response1 = self.app.get(f'{API_URL}/user/dummy_user_uuid/')
        self.assertEqual(response1.status_code, 404)

        response2 = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_OK)
        user_uuid = json.loads(response2.data)['user_uuid']

        response3 = self.app.get(f'{API_URL}/user/{user_uuid}/')
        self.assertEqual(response3.status_code, 200)
    
    def test_meeting_create(self):
        response = self.app.post(f'{API_URL}/meeting/create/', json=MEETING_CREATE_JSON_OK)
        self.assertEqual(response.status_code, 200)

        response = self.app.post(f'{API_URL}/meeting/create/', json=MEETING_CREATE_JSON_BAD)
        self.assertEqual(response.status_code, 400)
    
    def test_meeting_delete(self):
        response1 = self.app.delete(f'{API_URL}/meeting/dummy_meeting_uuid/?user_uuid=dummy_user_uuid')
        self.assertEqual(response1.status_code, 404)

        request_json = MEETING_CREATE_JSON_OK
        user_uuid = create_uuid()
        request_json['creator'] = user_uuid
        response2 = self.app.post(f'{API_URL}/meeting/create/', json=request_json)
        meeting_uuid = json.loads(response2.data)['meeting_uuid']

        response3 = self.app.delete(f'{API_URL}/meeting/{meeting_uuid}/?user_uuid=dummy_user_uuid')
        self.assertEqual(response3.status_code, 404)

        response4 = self.app.delete(f'{API_URL}/meeting/{meeting_uuid}/?user_uuid={user_uuid}')
        self.assertEqual(response4.status_code, 200)

    def test_meeting_get(self):
        response1 = self.app.get(f'{API_URL}/meeting/dummy_meeting_uuid/?user_uuid=dummy_user_uuid')
        self.assertEqual(response1.status_code, 404)

        request_json = MEETING_CREATE_JSON_OK
        user_uuid = create_uuid()
        request_json['creator'] = user_uuid
        response2 = self.app.post(f'{API_URL}/meeting/create/', json=request_json)
        meeting_uuid = json.loads(response2.data)['meeting_uuid']

        response3 = self.app.get(f'{API_URL}/meeting/{meeting_uuid}/?user_uuid=dummy_user_uuid')
        self.assertEqual(response3.status_code, 200)
        respJson3 = json.loads(response3.data)
        self.assertNotIn('name', respJson3)

        response4 = self.app.get(f'{API_URL}/meeting/{meeting_uuid}/?user_uuid={user_uuid}')
        self.assertEqual(response4.status_code, 200)
        respJson4 = json.loads(response4.data)
        self.assertIn('name', respJson4)
    
    def test_change_participation(self):
        response1 = self.app.post(
            f'{API_URL}/meeting/dummy_meeting_uuid/change_participation/?user_uuid=dummy_user_uuid',
            json={'status' : 'YES'})
        self.assertEqual(response1.status_code, 404)

        request_json = MEETING_CREATE_JSON_OK
        user_uuid = create_uuid()
        request_json['creator'] = user_uuid
        response2 = self.app.post(f'{API_URL}/meeting/create/', json=request_json)
        meeting_uuid = json.loads(response2.data)['meeting_uuid']

        response2 = self.app.post(
            f'{API_URL}/meeting/{meeting_uuid}/change_participation/?user_uuid={user_uuid}',
            json={'status' : 'YES'})
        self.assertEqual(response2.status_code, 200)

        response3 = self.app.get(f'{API_URL}/meeting/{meeting_uuid}/?user_uuid={user_uuid}')
        json1 = json.loads(response3.data)['participants']
        participation1 = None
        for value in json1:
            if value['user_uuid'] == user_uuid:
                participation1 = value['participation_status']
        self.assertEqual(participation1, 'YES')

        self.app.post(
            f'{API_URL}/meeting/{meeting_uuid}/change_participation/?user_uuid={user_uuid}',
            json={'status' : 'NO'})
        response4 = self.app.get(f'{API_URL}/meeting/{meeting_uuid}/?user_uuid={user_uuid}')
        json2 = json.loads(response4.data)['participants']
        participation2 = None
        for value in json2:
            if value['user_uuid'] == user_uuid:
                participation2 = value['participation_status']
        self.assertEqual(participation2, 'NO')
    
    def test_user_calendar(self):
        response1 = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_OK)
        user_uuid1 = json.loads(response1.data)['user_uuid']
        response2 = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_OK)
        user_uuid2 = json.loads(response2.data)['user_uuid']

        meeting_template = MEETING_CREATE_JSON_OK
        meeting_template['creator'] = user_uuid1
        meeting_template['participants'] = [user_uuid2]

        meeting_json_once = meeting_template
        meeting_json_once['repeat'] = 'ONCE'
        response3 = self.app.post(f'{API_URL}/meeting/create/', json=meeting_json_once)
        meeting_uuid1 = json.loads(response3.data)['meeting_uuid']

        meeting_json_daily = meeting_template
        meeting_json_daily['repeat'] = 'DAILY'
        response4 = self.app.post(f'{API_URL}/meeting/create/', json=meeting_json_daily)
        meeting_uuid2 = json.loads(response4.data)['meeting_uuid']

        meeting_json_weekly = meeting_template
        meeting_json_weekly['repeat'] = 'WEEKLY'
        response5 = self.app.post(f'{API_URL}/meeting/create/', json=meeting_json_weekly)
        meeting_uuid3 = json.loads(response5.data)['meeting_uuid']

        date_start = int(datetime.datetime.combine(
                datetime.date.fromisoformat(meeting_template['date']), 
                datetime.datetime.min.time()
            ).timestamp())
        date_end = int(datetime.datetime.combine(
                (datetime.date.fromisoformat(meeting_template['date']) + datetime.timedelta(days=32)),
                datetime.datetime.min.time()
            ).timestamp())
        response6 = self.app.get(
            f'{API_URL}/user/{user_uuid1}/calendar/?start_datetime={date_start}&end_datetime={date_end}')
        response7 = self.app.get(
            f'{API_URL}/user/{user_uuid2}/calendar/?start_datetime={date_start}&end_datetime={date_end}')
        self.assertEqual(response6.status_code, 200)
        self.assertEqual(response7.status_code, 200)

        json1 = json.loads(response6.data)
        json2 = json.loads(response7.data)

        self.assertDictEqual(json1, json2)

        def count(calendar_data, meeting_uuid):
            c = 0
            for value in calendar_data['calendar']:
                if value['uuid'] == meeting_uuid:
                    c += 1
            return c
        
        self.assertEqual(count(json1, meeting_uuid1), 1)
        self.assertEqual(count(json1, meeting_uuid2), 32)
        self.assertEqual(count(json1, meeting_uuid3), 5)
        self.assertEqual(json1['calendar'][-1]['start_datetime'], '2022-11-22T10:55:00')
    
    def test_best_time(self):
        response1 = self.app.post(f'{API_URL}/user/create/', json=USER_CREATE_JSON_OK)
        user_uuid1 = json.loads(response1.data)['user_uuid']

        user_2 = USER_CREATE_JSON_OK
        user_2['work_time_start'] = '11:00:00'
        user_2['work_time_end'] = '19:00:00'
        response2 = self.app.post(f'{API_URL}/user/create/', json=user_2)
        user_uuid2 = json.loads(response2.data)['user_uuid']

        meeting_template1 = MEETING_CREATE_JSON_OK
        meeting_template1['creator'] = user_uuid1
        meeting_template1['repeat'] = 'ONCE'

        meeting_template2 = MEETING_CREATE_JSON_OK
        meeting_template2['creator'] = user_uuid2
        meeting_template2['repeat'] = 'ONCE'
        
        self.app.post(f'{API_URL}/meeting/create/', json=meeting_template1)
        self.app.post(f'{API_URL}/meeting/create/', json=meeting_template2)

        request_json = FIND_BEST_TIME_JSON
        request_json['participants'].append(user_uuid1)
        request_json['participants'].append(user_uuid2)
    
        response3 = self.app.get(f'{API_URL}/find_best_time/', json=request_json)
        responseJson = json.loads(response3.data)
        print("DATA = ", responseJson)

        first_time = responseJson['best_times'][0]
        first_time_start = datetime.datetime.fromisoformat(first_time['start_datetime'])
        first_time_end = first_time_start + datetime.timedelta(seconds=first_time['max_duration'])

        meeting_time_end1 = (datetime.datetime.combine(
            datetime.date.today(),
            datetime.time.fromisoformat(meeting_template1['start_time'])) + datetime.timedelta(seconds=meeting_template1['duration'])).time()

        self.assertEqual(first_time_end.time(), datetime.time.fromisoformat(user_2['work_time_end']))
        self.assertEqual(first_time_start.time(), meeting_time_end1)

        last_time = responseJson['best_times'][-1]
        last_time_start = datetime.datetime.fromisoformat(last_time['start_datetime'])
        last_time_end = last_time_start + datetime.timedelta(seconds=last_time['max_duration'])

        self.assertEqual(last_time_end.time(), datetime.time.fromisoformat(user_2['work_time_end']))
        self.assertEqual(last_time_start.time(), datetime.time.fromisoformat(user_2['work_time_start']))

if __name__ == '__main__':
    unittest.main()
